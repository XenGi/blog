AUTHOR = 'XenGi'
SITENAME = 'XenGis blog'
SITEURL = ""
PATH = "content"
TIMEZONE = 'Europe/Berlin'
DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (
    ("Noisy Cricket", "https://gitlab.com/noisy_cricket"),
    ("mkbd65", "https://gitlab.com/mkbd65"),
)

# Social widget
SOCIAL = (
    ("gitlab", "https://gitlab.com/XenGi"),
    ("github", "https://github.com/XenGi"),
    ("mastodon", "https://chaos.social/@xengi"),
    ("matrix", "https://matrix.to/#/@xengi:xengi.de"),
)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

THEME = "flex"
PYGMENTS_STYLE = "monokai"

DEFAULT_CATEGORY = "misc"
DISPLAY_PAGES_ON_MENU = True
DISPLAY_CATEGORIES_ON_MENU = True

MARKDOWN = {
    "extension_configs": {
        "markdown.extensions.extra": {},
        "markdown.extensions.abbr": {},
        "markdown.extensions.fenced_code": {"lang_prefix": "language-"},
        "markdown.extensions.tables": {},
        "markdown.extensions.codehilite": {
            "css_class": "highlight",
            "pygments_style": "monokai",
            "lang_prefix": "language-",
        },
        "markdown.extensions.meta": {},
        #"markdown.extensions.nl2br": {},
        "markdown.extensions.sane_lists": {},
        "markdown.extensions.smarty": {},
    },
    "output_format": "html5",
}
PAGE_PATHS = ['pages']
STATIC_PATHS = ['images']
TYPOGRIFY = True

# Flex theme
## metadata for <head>
SITEDESCRIPTION = "XenGis blog"

THEME_COLOR = 'dark'
THEME_COLOR_AUTO_DETECT_BROWSER_PREFERENCE = True
THEME_COLOR_ENABLE_USER_OVERRIDE = True

## code highlight
PYGMENTS_STYLE = 'emacs'
PYGMENTS_STYLE_DARK = 'monokai'

## top menu
MENUITEMS = (
    # Home
    ("Archives", "/archives.html"),
    ("Categories", "/categories.html"),
    ("Tags", "/tags.html"),
)

SITETITLE = "XenGis blog"
SITESUBTITLE = "&#x2620;&#xFE0F; Hacker&#x2122;&#xFE0F; | &#x1F3CD;&#xFE0F; Petrol head | &#x1F680; Tech fondler"
SITELOGO = SITEURL + "/images/profile.png"
FAVICON = SITEURL + "/images/favicon.ico"

BROWSER_COLOR = "#333"
ROBOTS = "index, follow"

CC_LICENSE = {
    "name": "Creative Commons Attribution-NonCommercial 4.0 International",
    "version": "4.0",
    "slug": "by-nc",
    "icon": True,
    "language": "en_US"
}
from datetime import datetime
COPYRIGHT_YEAR = datetime.now().year
#  <p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://www.xengi.de">blog</a> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://www.xengi.de">XenGi</a> is licensed under <a href="https://creativecommons.org/licenses/by-nc/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-NC 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1" alt=""><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1" alt=""><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1" alt=""></a></p>

#STATIC_PATHS = ["extra/custom.css"]
#EXTRA_PATH_METADATA = {"extra/custom.css": {"path": "static/custom.css"}}
#CUSTOM_CSS = "static/custom.css"

MAIN_MENU = True

# TODO: remove ADD_THIS_ID
# TODO: remove DISQUS_SITENAME
# TODO: remove GOOGLE_ANALYTICS
# TODO: remove GOOGLE_TAG_MANAGER

PLUGINS = []

# Plugins
## Pelican-search Configuration ?
STORK_INPUT_OPTIONS = {"stemming": "English", "url_prefix": SITEURL}

