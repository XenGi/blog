---
title: Hello there!
date: 2025-01-14
category: misc
summary: I finally rebooted my blog and already have a lot of content in the pipeline.
---

I finally rebooted my blog and already have a lot of content in the pipeline. Sadly most of the old content is vanished. I could only find two old posts.

There will be a new Arch Linux installguiode 2025 coming soon. I'm also working on a pretty huge guide on how to setup kubernetes on NixOS. If there is interest I can also run down a small NixOS install Guide or how I use flakes even for my non Nix projects. Let me know if you would like that.

I'm still figuring out if I want to switch to [Hugo](https://gohugo.io) and move away from [Pelican](https://getpelican.com). Not because it's bad but because I wanted to try something new. In the end it's all the same anyway. One thing is for sure, I will remake the theme entirely. The Flex theme is nice, but I would like something that is optimized for my blog.
