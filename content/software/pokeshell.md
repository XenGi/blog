---
title: Pokéshell
date: 2016-01-01 23:30
category: software
tags: pokemon, shell, bash, fish, imagemagick, aur
slug: pokeshell
---

A friend of me had the idea of a shell displaying a random pokémon on launch. I liked the idea and couldn't resist to implement it over christmas.

<!-- PELICAN_END_SUMMARY -->

What the package does is, it downloads all pokémon images in a defined range which is 1-151 by default (I'm old these are the ones I know) and converts the images to escape sequences. These are saved in `/usr/share/pokeshell`.

Now when you open a new shell you could display one of them randomly by putting this code in your `~/.bashrc`:

```sh
cat /usr/share/pokeshell/\$((\$RANDOM % 151 + 1)).pokemon
```

Or if you use fish like me put this in your `~/.config/fish/functions/fish_prompt.fish`:

```sh
function fish_greeting
    set r (random)
    cat /usr/share/pokeshell/(math \"\$r % 151 + 1\").pokemon
end
```

The images are downloaded using the [Pokéapi][pokeapi]. For the conversation I use Imagemagick and a tool called [img2xterm][img2xterm].

You can find a package for all of this in the [AUR][aur-pokeshell].


[pokeapi]: http://pokeapi.co
[img2xterm]: https://github.com/rossy/img2xterm
[aur-pokeshell]: https://aur.archlinux.org/packages/pokeshell/

