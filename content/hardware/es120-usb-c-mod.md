---
title: ES120 USB-C mod
date: 2025-03-05
tags: screw driver, pcb, 3d print
category: hardware
summary: Add a USB-C port to your Miniware ES120 screwdriver.
---

I own a Miniware ES120 electronic screwdriver. It's one of the last devices in my household that still has a microUSB
connector which is pretty annoying. So I modded it to use USB-C. The mod is quite simple, you need a small PCB with the
USB-C port, a new 3d printed end cap that sticks out a little and you need to solder the connections to the original
PCB. In the end everything looks pretty neat and works really well.

Here's a little build guide.

# BOM

| Part              | Count | Schema Ref | Description | Mouser | LCSC |
| ----------------- | ----- | ---------- | ----------- | ------ | ---- |
| GCT USB4120-03-C  | 1     | J1         | USB-C port  | [640-USB4120-03-C ](https://mou.sr/3Whgqrz) | [C3445864](https://www.lcsc.com/product-detail/USB-Connectors_Global-Connector-Technology-USB4120-03-C_C3445864.html) |
| 5.1 kOhm Resistor | 2     | R1,R2      | 0603/1608   | [667-ERJ-3EKF5101V](https://mou.sr/41IiRG1) | [C2907114](https://www.lcsc.com/product-detail/Chip-Resistor-Surface-Mount_FOJAN-FRC0603J512-TS_C2907114.html) |

You can get the parts on [Mouser](https://eu.mouser.com) or [LCSC](https://www.lcsc.com). Get the PCBs from
[Aisler](https://aisler.net/) or [JLCPCB](https://jlcpcb.com/). The 3D printed end cap can be ordered at
[JLC3D](https://jlc3dp.com/). They can even assemble everything for you if the parts are too small for your taste and
you don't want to solder it yourself.

The endcap can also be 3D printed at home, at any other 3D printing service or at your local hackspace.

# Additional parts

To connect the breakout board to the PCB you will need some enameled wire or single core cable strand.

# PCB

![front](https://gitlab.com/XenGi/es120-usb-c-mod/-/raw/main/es120-usb-c-front.png)
![back](https://gitlab.com/XenGi/es120-usb-c-mod/-/raw/main/es120-usb-c-back.png)

The gerber files are zipped in the `production` folder. Upload them to the PCB fab and make sure the PCB has a thickness
of only 1mm. There is really not much space in the housing.

# Endcap

![endcap](https://gitlab.com/XenGi/es120-usb-c-mod/-/raw/main/cad/rev2/endcap.png)

You can find the 3D model in the [repository](https://gitlab.com/XenGi/es120-usb-c-mod) or on
[Printables](https://www.printables.com/model/1216140-endcap-for-miniware-es120-usb-c-mod).

If you want to print it yourself, I'd recommend using a 0.25mm nozzle. I printed it in
[Prusament PETG Prusa Galaxy Black](https://www.prusa3d.com/product/prusament-petg-prusa-galaxy-black-1kg/) and it came
out pretty nice.

# Integration

1. Unscrew the end piece of the screwdriver
2. Unscrew display with Torx T3
3. Remove display cover
4. Remove PCB
5. Desolder microUSB port
6. Solder wires from PCB to USB-C adapter
7. Insert PCB back into the case
8. Put display cover back on
8. Put adapter PCB into endcap
9. Put endcap into the slots so it fits snug
10. Screw on the metal end piece
11. Put the display screw back in
12. Profit!

Try to charge the screwdriver and connect it to a PC to check if power and data lines work again.

