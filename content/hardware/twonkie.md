---
title: USB-PD Protocol analyser
date: 2023-08-02
category: hardware
tags: mkbd65, twinkie, twonkie, usb, usb-c, usb-pd, ct-vpd, 3d print, pcb
summary: I build a USB-PD analyzer based on the Google Twinkie.
---

While working on my [mkbd65][mkbd65] project I needed a way to check what went over the USB-C `CC` lines, especially within the USB PD protocol. My idea is to build mkbd65 as a CT-VPD, so this part is crucial to it's success. I found a really cool open source project by Google, called [Twinkie][twinkie] which solved this issue. Problem is that it is kinda hard to assemble with very little SMD components and as a 6 layer PCB it's also a bit expensive to produce.

Happily someone took that design and produced the [Twonkie][twonkie], which is a simplified version of the Twinkie with less PCB layers and bigger SMD components.

There was only one thing I wanted to change. The USB-C connectors on the Twonkie where hard to solder because they where not used as designed. So I forked the project and changed them to different connectors that are better suited. You can find my fork here: [gitlab.com/XenGi/Twonkie](https://gitlab.com/XenGi/Twonkie/-/tree/v2.1?ref_type=tags)

All parts and the PCB can be ordered at JLCPCB which makes it very cheap to produce. I also created a 3D printable case for my version which you can find on [Printables](https://www.printables.com/model/562407-twonkie-v21-case).

The documentation describes how to use it together with Sigroks PulseView. By now the board should have upstream support. If not, I build a version of `libsigrok` with support for the Twinkie and Twonkie boards. You can find it in the [AUR](https://aur.archlinux.org/packages/libsigrok-chromium-twinkie).

*[USB PD]: USB Power Delivery
*[CT-VPD]: Charge Through VCONN Powered Device

[mkbd65]: https://mkbd65.xengi.de
[twinkie]: https://www.chromium.org/chromium-os/developer-library/guides/hardware-schematics/twinkie/
[twonkie]: https://github.com/dojoe/Twonkie

