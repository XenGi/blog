{
  description = "XenGis nix configurations";

  inputs = {
    nixpkgs.url = github:NixOS/nixpkgs/nixos-24.11;
  };

  outputs = { self, nixpkgs }:
    let
      system = "x86_64-linux";
      #pkgs = nixpkgs.legacyPackages.x86_64-linux;
      pkgs = import nixpkgs { inherit system; };
    in
    {
      formatter.x86_64-linux = pkgs.nixpkgs-fmt;
      devShells.x86_64-linux.default = pkgs.mkShell {
        packages = [
          (pkgs.python3.withPackages (ps: [
            ps.pelican
            ps.markdown
            #ps.pelican-sitemap
            #ps.pelican-related-posts
            #ps.pelican-neighbors
            ps.typogrify
            ps.beautifulsoup4
          ] ++ ps.pelican.optional-dependencies.markdown
          ))
        ];
      };
    };
}
